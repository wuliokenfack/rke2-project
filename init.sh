export KUBECONFIG=/etc/rancher/rke2/rke2.yaml
export PATH=$PATH:/var/lib/rancher/rke2/bin
alias k=kubectl


aws secretsmanager get-secret-value --secret-id vdms-test-cluster-management-ssh | jq .SecretString -r | jq .private_key -r > /tmp/pem.key
chmod 600 /tmp/pem.key
ssh ubuntu@10.2.20.12 -i /tmp/pem.key 



#upgrade path
https://github.com/rancher/system-upgrade-controller/blob/master/pkg/apis/upgrade.cattle.io/v1/types.go